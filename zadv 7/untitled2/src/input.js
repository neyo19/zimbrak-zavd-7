import React from 'react';
import  {findVowels , palindrome, alphabeta , NaT} from './functions';
import './App.css';
class NameForm extends React.Component {



    constructor(props) {
        super(props);
        this.state = {value: '' , vowl: '' , arr: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeVowl = this.handleChangeVowl.bind(this)
        this.handleChangeArr = this.handleChangeArr.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});

    }

    handleChangeVowl(event) {
        this.setState({vowl: event.target.value});

    }

    handleChangeArr(event) {
        this.setState({arr: event.target.value});

    }

    handleSubmit(event) {

        event.preventDefault();
    }



    render() {
        return (
            <div id='main'>

            <form onSubmit={this.handleSubmit}>
                <h1> Task 7 </h1>
                <h3 id="func">PALINDROME</h3>
                <label>
                    <input id={'inp'} type="text" value={this.state.value} onChange={this.handleChange} />

                </label>
                <p>Is "{this.state.value}" palidrome? </p>
                <p>
                    Result: {palindrome(this.state.value)?
                    <span style={{color:'green'}}> true </span> :
                    <span style={{color:'red'}}> false</span>}
                </p>
                <label>
                    <input id='inp' type="text" value={this.state.vowl} onChange={this.handleChangeVowl} />
                </label>
                <h3 id="func">VOWELS</h3>
                <p>  <p id={'result'}> "{this.state.vowl}" has {findVowels(this.state.vowl)} Vowels  </p> </p>
                <label>
                    <h3 id="func">ALPHABETA</h3>
                    <input id='inp'  type="text" value={this.state.arr} onChange={this.handleChangeArr} />
                </label>
                <p>your number "{this.state.arr}"  </p>   <p id={'result'}>  {alphabeta(this.state.arr)} </p>
                <p> {NaT}</p>
            </form>
            </div>
        );
    }
}

export default NameForm;

