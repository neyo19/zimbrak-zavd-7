import {describe, expect, test} from '@jest/globals';
import  palindrome from './functions';
import  findVowels from './functions';
import  alphabeta from './functions';
import  NaT from './functions';


describe('PALINDROME', () =>{

  test('testodin', () => {
    expect(palindrome('hi')).toBeTruthy();
  })

  test('testDwa', () => {
    expect(palindrome('2')).toBeFalsy();
  })

  test('testtri', () => {
    expect(palindrome('b')).toBeDefined();
  })

  test('testchotiri', () => {
    expect(palindrome('hi')).not.toBeUndefined();
  })

  test('testPyat', () => {
    expect(palindrome('hi')).not.toBeNaN();
  })
});


describe('FINDVOWELS', () =>{

  test('test1', () => {
    expect(findVowels('i')).toBeTruthy();
  })

  test('test2', () => {
    expect(findVowels('h')).toBeFalsy();
  })

  test('test3', () => {
    expect(findVowels('azbuka')).toBeDefined();
  })

  test('test4', () => {
    expect(findVowels('hi')).not.toBeUndefined();
  })

  test('test5', () => {
    expect(findVowels('hi')).not.toBeNaN();
  })
});


describe('ALPHABETA', () => {

  test('testone', () => {
    expect(alphabeta('hi')).toBeTruthy();
  })

  test('testtwo', () => {
    expect(alphabeta('h')).toBeFalsy();
  })

  test('testthree', () => {
    expect(alphabeta('azbuka')).toBeDefined();
  })

  test('testfour', () => {
    expect(alphabeta('hi')).not.toBeUndefined();
  })

  test('testfive', () => {
    expect(alphabeta('hi')).not.toBeNaN();
  })
});

describe('NaT', () => {
  test('testGet', () => {
    expect(NaT('')).toBeFalsy();
  })
});